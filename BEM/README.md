# Dinamcika Development
## CSS styling - BEM

Здесь приведена полезная информация о методологии BEM.

#### Полезные материалы:
* [Методология ВЕМ - Быстрый старт](https://ru.bem.info/methodology/quick-start/)
* [Методология БЭМ и обзор БЭМ платформы - Владимир Гриненко](https://www.youtube.com/watch?v=NoZu4hVxTCQ)
* [БЭМ мастер класс - Артём Курбатов](https://www.youtube.com/watch?v=ftJjkHGcpgo)
