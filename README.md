# Dinamcika Development
## CSS styling
Здесь приведены правила и рекомендации для разработчиков компании Dinamicka Development касательно написания и оформления CSS кода.

### 1. Коментарии

Рекомендуется разбивать код на логические блок, отделённые друг от друга коментариями вида:

```css
/*======================== Name of current CSS rules block ===========================*/
```

Такой вид коментариев обеспечивает легкую навигацию по файлу, и сокращает время на поиск необходимого блока.

### 2. Структура файла CSS

Файл CSS должен быть написан в следущем стиле

```css
/*  File documentation
    Author: Name

    1.Block 1
    2.Block 2
*/

/*=================== 1. Block 1 ====================*/
#block1{
    width:10px;
    height:10px;
}
@media screen and (max-width:1200px){
    #block1{ width: 7px; }
}
@media screen and (max-width:992px){
    #block1{ width: 5px; }
}
@media screen and (max-width:768px){
    #block1{ width: 3px; }
}
/*=================== 2.  Block 2 ====================*/
#block2{
    width:10px;
    height:10px;
}
@media screen and (max-width:1200px){
    #block2{ width: 7px; }
}
@media screen and (max-width:992px){
    #block2{ width: 5px; }
}
@media screen and (max-width:768px){
    #block2{ width: 3px; }
}
```